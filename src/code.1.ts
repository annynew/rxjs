import { Observable } from "rxjs/Observable";
 import {fromEvent} from "rxjs/observable/fromEvent";

var observable = fromEvent(document, 'mousemove');

setTimeout(() => {
   var subscription2 = observable.subscribe(
       (x:any) => addItem(x)
   );
}, 2000);   

function addItem(val:any) {
    var node = document.createElement("li");
    var textNode = document.createTextNode(val);
    node.appendChild(textNode);
    document.getElementById("output").appendChild(node);
}