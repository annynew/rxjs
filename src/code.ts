import { Observable } from "rxjs/Observable";
import {Subject } from "rxjs/Subject";

var subject = new Subject()

subject.subscribe(
    data => addItem("Observer 1: " + data),
    err => addItem(err),
    () => addItem('Observer 1 Completed')
)

function addItem(val:any) {
    var node = document.createElement("li");
    var textNode = document.createTextNode(val);
    node.appendChild(textNode);
    document.getElementById("output").appendChild(node);
}